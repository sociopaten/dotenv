## usage

```require('dotenv')()```

Create a ```.env``` file in the root directory of your project.
Add environment variables in the the form of ```NAME="value"```

Access values from ```process.env.NAME```

Heavily inspired by [https://crates.io/crates/dotenv](https://crates.io/crates/dotenv)
